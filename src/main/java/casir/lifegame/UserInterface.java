package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class UserInterface{

    public static void displayGameBoard(Board board){
        System.out.print("\033[H\033[2J");
        System.out.flush();
        String toDisplay;
        for (int line_index=0; line_index<20; line_index++){
            toDisplay = "";
            for (int column_index=0; column_index<20; column_index++) {
                if (board.content[line_index][column_index]){
                    toDisplay += "#";
                } else {
                    toDisplay += "_";
                }
            }
            System.out.println(toDisplay);
        }
    }

    public static void displayEndMessage() {
        System.out.println("Game over");
    }

}
