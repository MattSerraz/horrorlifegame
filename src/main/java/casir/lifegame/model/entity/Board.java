package casir.lifegame.model.entity;

public class Board {

    public boolean[][] content;

    public Board(int size){
        this.content = new boolean[size][size];
        boolean[] line;
        for (int line_index=0; line_index<size; line_index++){
            line = new boolean[size];
            for (int column_index=0; column_index<size; column_index++) {
                line[column_index] = false;
            }
            this.content[line_index] = line;
        }
    }

    public boolean eq(Board board2) {
        for (int line_index=0; line_index<this.content.length; line_index++){
            for (int column_index=0; column_index<this.content.length; column_index++) {
                if (this.content[line_index][column_index] != board2.content[line_index][column_index]){
                    return false;
                }
            }
        }
        return true;
    }

    public Board clone(){
        Board clone = new Board(this.content.length);
        for (int line_index=0; line_index<this.content.length; line_index++){
            for (int column_index=0; column_index<this.content.length; column_index++) {
                clone.content[line_index][column_index] = this.content[line_index][column_index];
            }
        }
        return clone;
    }
}
