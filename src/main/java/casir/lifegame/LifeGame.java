package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class LifeGame {

    public static void main(String[] args){
        LifeGame game = new LifeGame();
    }

    public LifeGame(){
        Board grid2 = new Board(20);
        Board grid = new Board(20);
        grid.content[17][17] = true;
        grid.content[18][17] = true;
        grid.content[17][18] = true;
        grid.content[19][18] = true;
        grid.content[17][19] = true;

        while(!grid.eq(grid2)){
            UserInterface.displayGameBoard(grid);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            grid2 =  grid.clone();
            grid = new Board(20);
            start_lifeGame(grid, grid2);

        }

        UserInterface.displayEndMessage();
    }

    public void start_lifeGame(Board grid, Board grid2){
        for (int line_index=0; line_index<20; line_index++){
            for (int column_index=0; column_index<20; column_index++) {
                int nb = 0;
                for (int vertical_shift= -1; vertical_shift <= 1; vertical_shift++){
                    for (int horizontal_shift = -1; horizontal_shift <= 1; horizontal_shift++){
                        if (!(vertical_shift== 0 && horizontal_shift==0)){
                            if (line_index+horizontal_shift >= 0 && line_index+horizontal_shift < 20 && column_index + vertical_shift >= 0 && column_index+vertical_shift < 20){
                                boolean ok =  grid2.content[line_index+horizontal_shift][column_index+vertical_shift];
                                if (ok){
                                    nb ++;
                                }
                            }
                        }
                    }
                }
                if ( grid2.content[line_index][column_index]){
                    if (nb < 2 || nb > 3){
                         grid.content[line_index][column_index] = false;
                    } else {
                         grid.content[line_index][column_index] = true;
                    }
                } else {
                    if (nb == 3){
                         grid.content[line_index][column_index] = true;
                    } else {
                         grid.content[line_index][column_index] = false;
                    }
                }
            }
        }
    }
}
